#include <stdio.h>
#include <limits.h> 
#include <math.h>

/* function prototypes */
long maxlong(void);
long factorial(long n);
double upper_bound(long n);


int main(void) {
    long i;

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */

    for (i=0; i<10; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
    }
	return 0;
}

long factorial(long n){
	if(n<0){
		return -2;
	} 
	if(n > 12){
		return -1;
	}
  	if(n!=1 && n!=0 && n>0){
    	return n*factorial(n-1);
	}
	else
		return 1;
}

double upper_bound(long n){
	double fraction, power;
	fraction = n / 2.0;
	power = pow(fraction, n);
	
	if (power < factorial(n)){
		return factorial(n);
	}
	else {
		return power;
	}
   
    return 0;
}

long maxlong(void){
	return  LONG_MAX;

}




