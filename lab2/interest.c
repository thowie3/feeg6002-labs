#include <stdio.h>

int main(void) {
	double s = 1000, debt = s, rate = 0.03;
	int month;
	double interest, total_interest = 0, frac;
	for (month = 1; month < 25; month++) {
		interest = debt * rate;
		debt *= (1+rate);
		total_interest += interest;
		frac = (total_interest / s) * 100;  
		printf("month %2d: debt=%7.2f, interest=%4.2f, total_interest=%7.2f, frac=%6.2f%%\n", month, debt, interest, total_interest, frac); 
		}
	return 0;
	
}

