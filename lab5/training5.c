#include <stdio.h>

#define MAXLINE 1000 /* maximum input line size */

/*function prototypes*/
long string_length(char s[]);

/* print length of line */
int main(void){

  	char s1[]="Hello";
  	char s2[]="x";
	char s3[]="line 1\tline 2\n";

  	printf("%20s | %s\n", "string_length(s)", "s");
  	printf("%20ld | %s\n", string_length(s1), s1);
  	printf("%20ld | %s\n", string_length(s2), s2);
	printf("%20ld | %s\n", string_length(s3), s3);
  
	return 0;
}

/* reads a line in s and returns length */
long string_length(char s[]) {
  char c[MAXLINE];
  long i=0;
  
  while ((c[i] = s[i]) != '\0')
  	++i;
  return i;
}



