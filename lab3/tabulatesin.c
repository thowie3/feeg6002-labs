#include <stdio.h>
#include <math.h>

#define XMIN 1
#define XMAX 10
#define N 10 

int main(void) {
	double x, y, a;
	a = ((XMAX-XMIN) / ((double)N-1));
	
	for (x = XMIN; x <= XMAX; x += a) {
		y = sin(x);
		printf("%.6f %.6f\n", x, y);
	}
	
	return 0;
}	 

			

