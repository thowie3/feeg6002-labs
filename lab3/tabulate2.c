#include <stdio.h>
#include <math.h>

#define XMIN 1.0
#define XMAX 10.0
#define N 10 

int main(void) {
	double x, y, z, a;
	a = ((XMAX-XMIN) / ((double)N-1));
	
	for (x = XMIN; x <= XMAX; x += a) {
		y = sin(x);
		z = cos (x);
		printf("%.6f %.6f %.6f\n", x, y, z);
	}
	
	return 0;
}	 

			

